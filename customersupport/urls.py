from django.urls import path
from . import views
from .views import add_subscribe

urlpatterns = [
    path('CustomerSupport', add_subscribe, name='customersupport'),
]
