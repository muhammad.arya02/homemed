from django.shortcuts import render, redirect
from .models import subscribe
from .forms import SubscribeForm

# Create your views here.
def home(request):
    return render(request, 'customersupport.html')

def add_subscribe(request):
    context ={}
  
    # create object of form
    form = SubscribeForm(request.POST or None, request.FILES or None)
    if request.method == "POST":  
    # check if form data is valid
        if form.is_valid():
            # save the form data to model
            form.save()
            return redirect("/")
  
    context['form']= form
    return render(request, "customersupport.html", context)